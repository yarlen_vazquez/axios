import express from 'express';
import cors from 'cors';

import routeAlumno from './router/alumnos.routes.js';

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use(routeAlumno.router);

const port = 3000;
app.listen(port, () => {
  console.log(`Server en puerto: ${port}`);
});
