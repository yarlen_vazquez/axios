import conexion from './conexion.js';

export const alumnosDb = {};

alumnosDb.insertar = (alumno) =>
  new Promise((resolve, reject) => {
    conexion.sqlConnection.query(
      'INSERT INTO alumno SET ?',
      [alumno],
      (err, res) => {
        if (err) return reject(err);

        resolve({
          id: res.insertId,
          matricula: alumno.matricula,
          nombre: alumno.nombre,
          domicilio: alumno.domicilio,
          sexo: alumno.sexo,
          especialidad: alumno.especialidad,
        });
      }
    );
  });

alumnosDb.mostrarTodos = () =>
  new Promise((resolve, reject) => {
    conexion.sqlConnection.query('SELECT * FROM alumno', (err, res) => {
      if (err) reject(err);
      resolve(res);
    });
  });

// Buscar alumnos por matricula
alumnosDb.buscarMatricula = (matricula) =>
  new Promise((resolve, reject) => {
    conexion.sqlConnection.query(
      'SELECT * FROM alumno WHERE matricula = ?',
      [matricula],
      (err, res) => {
        if (err) return reject(err);
        if (res.length <= 0) reject({ message: 'No exists' });
        resolve(res);
      }
    );
  });

alumnosDb.borrar = (id) =>
  new Promise((resolve, reject) => {
    conexion.sqlConnection.query(
      'DELETE FROM alumno WHERE id = ?',
      [id],
      (err, res) => {
        if (err) return reject(err);
        if (res.affectedRows <= 0) return reject({ message: 'No exists' });
        resolve({
          message: `User where id = ${id} deleted succesfully.`,
        });
      }
    );
  });

alumnosDb.actualizar = (alumno) =>
  new Promise((resolve, reject) => {
    conexion.sqlConnection.query(
      'UPDATE alumno SET ? WHERE id = ?',
      [alumno, alumno.id],
      (err, res) => {
        if (err) return reject(err);
        if (res.affectedRows <= 0) return reject({ message: 'No exists' });
        resolve({
          message: `User where id = ${alumno.id} update succesfully.`,
          matricula: alumno.matricula,
          nombre: alumno.nombre,
          domicilio: alumno.domicilio,
          sexo: alumno.sexo,
          especialidad: alumno.especialidad,
        });
      }
    );
  });

export default { alumnosDb };
