import mysql from 'mysql2';

export const sqlConnection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '12345',
  database: 'sistema',
});

sqlConnection.connect((err) => {
  if (err) {
    return console.log(`Error conexion BD: ${err}`);
  }
  console.log('Base de datos conectada con exito...');
});

export default { sqlConnection };
