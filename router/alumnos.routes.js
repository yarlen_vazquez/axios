import express from 'express';
import { alumnosDb } from '../models/alumno.js';

const router = express.Router();

router.post('/insertar', (req, res) => {
  const alumno = req.body;
  alumnosDb
    .insertar(alumno)
    .then((data) => res.json(data))
    .catch((err) => res.status(404).send(err.message));
});

router.get('/mostrartodos', (req, res) => {
  alumnosDb
    .mostrarTodos()
    .then((data) => res.json(data))
    .catch((err) => res.status(404).send(err.message));
});

router.get('/buscar/:matricula', (req, res) => {
  const matricula = req.params.matricula;
  alumnosDb
    .buscarMatricula(matricula)
    .then((data) => res.json(data))
    .catch((err) => res.status(404).send(err.message));
});

router.delete('/borrar/:id', (req, res) => {
  const id = req.params.id;
  alumnosDb
    .borrar(id)
    .then((data) => res.json(data))
    .catch((err) => res.status(404).send(err.message));
});

router.put('/actualizar', (req, res) => {
  const alumno = req.body;
  alumnosDb
    .actualizar(alumno)
    .then((data) => res.json(data))
    .catch((err) => {
      res.status(404).send(err.message);
    });
});

export default { router };
