CREATE DATABASE IF NOT EXISTS sistema;

USE sistema;

CREATE TABLE IF NOT EXISTS alumno(
  id int NOT NULL AUTO_INCREMENT,
  matricula varchar(45) UNIQUE NOT NULL,
  nombre VARCHAR(45) NOT NULL,
  domicilio VARCHAR(45) NOT NULL,
  sexo VARCHAR(1) NOT NULL,
  especialidad VARCHAR(45) NOT NULL,
  PRIMARY KEY (id)
);
